#!/bin/bash

if [ "$#" -ne "2" ]
then
    echo -e "\e[32mUsage:\e[1m $0 SDIMG_SRC DEVICE_DST\e[0m"
    echo "Example: $0 wpe-raspbian.img /dev/mmcblk0"
    exit -1
fi

SDIMG=$1
OUTPUT=$2
# OUTPUT="/dev/mmcblk0"
# OUTPUT="/dev/sdb"
if ! [[ -b ${OUTPUT} ]]
then
	echo "error: ${OUTPUT} is not a block device"
    exit 1
fi

echo "Start: $(date)"
echo "Image md5sum: $(md5sum ${SDIMG})"
umount ${OUTPUT}?
umount ${OUTPUT}p?
dd status=progress if=${SDIMG}  of=${OUTPUT}  bs=4096
echo "End: $(date)"
