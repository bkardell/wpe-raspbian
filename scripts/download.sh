#!/bin/bash -e

IMAGE=wpe-raspbian.img

mkdir -p build
cd build

wget -O raspbian https://downloads.raspberrypi.org/raspbian_latest
