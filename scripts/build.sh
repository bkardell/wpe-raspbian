#!/bin/bash -e

function clean {
    # unmount everything
    ( umount rootfs/{dev/pts,dev/null,dev/random,dev/urandom,dev,sys,proc,boot,}  || true ) > /dev/null 2>&1
	( umount rootfs/var/cache || true ) > /dev/null 2>&1
    ( umount -l rootfs || true ) > /dev/null 2>&1
    # unmount loop device
    ( kpartx -d /dev/loop${LOOP_NUMBER} || true ) > /dev/null 2>&1
    ( losetup -d /dev/loop${LOOP_NUMBER} || true ) > /dev/null 2>&1
	( rmdir rootfs || true ) > /dev/null 2>&1
}

trap clean EXIT

IMAGE=wpe-raspbian.img

mkdir -p build
cd build

mkdir rootfs
mkdir -p cache
rm -rf *img
unzip raspbian
mv *img ${IMAGE}
dd if=/dev/zero bs=1M count=512 >> ${IMAGE}

# resize the partition
LOOP_DEVICE=$(losetup -f)
echo "Loop device: $LOOP_DEVICE"
losetup ${LOOP_DEVICE} ${IMAGE}
LOOP_PARTITION=$(kpartx -v -a ${IMAGE} | awk '{print $3}' | grep p2)
LOOP_NUMBER=$(echo $LOOP_PARTITION | cut -d 'p' -f 2)
echo "Loop number: ${LOOP_NUMBER} | Raspbian loop partition: ${LOOP_PARTITION}"
parted -- /dev/loop${LOOP_NUMBER} print
parted -- /dev/loop${LOOP_NUMBER} resizepart 2 -1s
parted -- /dev/loop${LOOP_NUMBER} print

# check the file system
e2fsck -f /dev/mapper/loop${LOOP_NUMBER}p2

# expand the filesystem
resize2fs /dev/mapper/loop${LOOP_NUMBER}p2

# mount partition
mount -t ext4 -o rw /dev/mapper/loop${LOOP_NUMBER}p2 rootfs
mount -t vfat -o rw /dev/mapper/loop${LOOP_NUMBER}p1 rootfs/boot

# mount binds
mount --bind cache rootfs/var/cache
mount --bind /dev rootfs/dev/
mount --bind /sys rootfs/sys/
mount --bind /proc rootfs/proc/
mount --bind /dev/pts rootfs/dev/pts
# mount --bind /dev/zero rootfs/dev/zero 
mount --bind /dev/null rootfs/dev/null
# mount --bind /dev/random rootfs/dev/random 
# mount --bind /dev/urandom rootfs/dev/urandom 

# ld.so.preload fix
sed -i 's/^/#/g' rootfs/etc/ld.so.preload

# copy qemu binary
cp /usr/bin/qemu-arm-static rootfs/usr/bin/

# copy setup
cp -a ../setup rootfs/

# chroot to raspbian
chroot rootfs /setup/setup-wpe-raspbian.sh

# Removed copied files
rm -rf rootfs/setup
rm -f rootfs/usr/bin/qemu-arm-static

# ld.so.preload restore
sed -i 's/^#//g' rootfs/etc/ld.so.preload

clean

# create a ZIP
zip -r ${IMAGE}.zip ${IMAGE}
MD5=$(md5sum ${IMAGE}.zip)
echo "${MD5}" > ${IMAGE}.zip.md5
echo "${IMAGE}.zip md5sum: ${MD5}"

