#! /bin/bash -xe

# Update APT sources list
cp /etc/apt/sources.list /etc/apt/sources.list.orig
install -m 644 -o root -g root /setup/cfg/sources.list /etc/apt/
apt-get update

# Update system
apt-get remove  -y libgcc-8-dev

# Install Weston and Cog
apt-get install -y raspi-config mesa-utils \
		           libwayland-egl1 weston \
                   cog \
				   python3-gi \
                   gir1.2-gtk-3.0 \
				   gir1.2-webkit2-4.0 \
				   libwebkit2gtk-4.0

# # Restoring the originial sources.list
cp /etc/apt/sources.list.orig /etc/apt/sources.list
apt-get update

# Copy the launcher for Cog
install -m 755 -o root -g root /setup/bin/cog-open /usr/local/bin/
install -m 755 -o root -g root /setup/bin/cog-ping /usr/local/bin/
install -m 755 -o root -g root /setup/bin/cog-remote-inspector-viewer /usr/local/bin/
install -m 755 -o root -g root /setup/bin/run-cog /usr/local/bin/
cp -a /setup/data/icons/wpe  /usr/share/icons/
cp -a /setup/data/wallpapers/wpe  /usr/share/wallpapers/

# Copy the launcher for Weston
install -m 755 -o root -g root /setup/bin/run-weston  /usr/local/bin/
install -m 755 -o root -g root /setup/bin/run-weston-x11  /usr/local/bin/
install -m 755 -o root -g root /setup/bin/stop-weston  /usr/local/bin/
install -m 644 -o root -g root /setup/cfg/weston.service /lib/systemd/system/
install -m 755 -o pi -g pi -d /home/pi/.config/
install -m 644 -o pi -g pi /setup/cfg/weston.ini /home/pi/.config/
install -m 755 -o pi -g pi -d /home/pi/Desktop/
install -m 755 -o pi -g pi /setup/data/org.igalia.WPEWeston.desktop /home/pi/Desktop/
install -m 755 -o pi -g pi /setup/data/org.igalia.WPEWestonX11.desktop /home/pi/Desktop/

# Copy pcmanfm configuration
install -m 755 -o pi -g pi -d /home/pi/.config/pcmanfm/LXDE-pi/
install -m 644 -o pi -g pi /setup/cfg/pcmanfm/LXDE-pi/desktop-items-0.conf /home/pi/.config/pcmanfm/LXDE-pi/
install -m 755 -o pi -g pi -d /home/pi/.config/lxpanel/LXDE-pi/panels/
install -m 644 -o pi -g pi /setup/cfg/lxpanel/LXDE-pi/panels/panel /home/pi/.config/lxpanel/LXDE-pi/panels/

# Copy Bash profile
install -m 644 -o pi -g pi /setup/cfg/bashrc  /home/pi/.bashrc

# Copy RPI config with Broadcom VC4 driver enabled
install -m 755 -o root -g root /setup/cfg/config.txt /boot/config.txt

