**Disclaimer:** This image is for demostration purpose, some issues or
unexpected behaviors can happen using it.


## What is .. ?

* [Raspian](https://www.raspberrypi.org/): Raspbian is a Debian-based
  computer operating system for Raspberry Pi.
* [WebKit](https://webkit.org/): WebKit is the web browser engine used
  by Safari, Mail, App Store, and many other apps on macOS, iOS, and Linux
* [WPE](https://webkit.org/wpe/): WPE is the reference WebKit port for
  embedded and low-consumption computer devices. It has been designed from
  the ground-up with performance, small footprint, accelerated content
  rendering, and simplicity of deployment in mind, bringing the excellence
  of the WebKit engine to countless platforms and target devices.
* [Wayland](https://wayland.freedesktop.org/): Wayland is intended as a
  simpler replacement for X, easier to develop and maintain.
* [Weston](https://github.com/wayland-project/weston): Weston is the
  reference implementation of a Wayland compositor, as well as a useful
  environment in and of itself.
* [Cog](https://github.com/Igalia/cog): WPE launcher and webapp container.
  Cog is a small single “window” launcher for the WebKit WPE port. It is
  small, provides no user interface, and is suitable to be used as a Web
  application container. The “window” may be fullscreen depending on the
  WPE backend being used.


## Build instructions

```
git clone git@gitlab.com:browsers/wpe-raspbian.git
cd wpe-raspbian
./scripts/download.sh
sudo ./scripts/build.sh
sudo ./scripts/copy2mmc.sh  build/wpe-raspbian.img  /dev/sdb
```


## Using the prebuilt binary image

```
wget https://wk-contrib.igalia.com/debian/imapes/wpe-raspbian.img.zip
wget https://wk-contrib.igalia.com/debian/images/wpe-raspbian.img.zip.md5
unzip wpe-raspbian.img.zip
sudo dd if=wpe-raspbian.img of=/dev/<SD card>
```

## Running the image

![Running WPE in Raspbian](/data/images/example-1.gif "Running WPE in Raspbian")


## How works

### Using WPE over Weston in the top of a X11 server using the Weston X11 backend

Currently Raspbian still uses Xorg as the graphical server. WebKit WPE
runs in the top of a Wayland compositor. The easy way to run WPE in Raspbian
for evaluation is running it in the top of a Wayland compositor like Weston.

This Raspbian's customized image contains a presinstalled Weston server that
we use as the Wayland environment to run WPE.

```
 /usr/sbin/lightdm
  \_ /usr/lib/xorg/Xorg :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
  \_ lightdm --session-child 13 16
      \_ /usr/bin/lxsession -s LXDE-pi -e LXDE
          \_ /usr/bin/ssh-agent x-session-manager
          \_ pcmanfm --desktop --profile LXDE-pi
              \_ /bin/bash /usr/local/bin/run-weston-x11
                  \_ weston -c /home/pi/.config/weston.ini
                      \_ /usr/lib/arm-linux-gnueabihf/weston-keyboard
                      \_ /usr/lib/arm-linux-gnueabihf/weston-desktop-shell
                          \_ /bin/bash /usr/local/bin/run-cog
                              \_ cog -P fdo https://www.igalia.com/project/wpe
                                  \_ /usr/lib/arm-linux-gnueabihf/wpe-webkit-1.0/WPEWebProcess 7 19
                                  \_ /usr/lib/arm-linux-gnueabihf/wpe-webkit-1.0/WPENetworkProcess 8 19

```

### Using WPE directly over Weston

The image also offers the choice to run a Weston Wayland compositor.
In this mode, the current X11 session will inmediately terminate and
an new Weston session will start.

```
$ run-weston
```

This method is the better option to explore WPE if you want to check
the real performance of the browser.


```
/usr/bin/weston-launch -u bot -- --log=/tmp/weston.log --backend=drm-backend.so --tty 1 --idle-time=0
 \_ /usr/bin/weston --log=/tmp/weston.log --backend=drm-backend.so --tty 1 --idle-time=0
     \_ /usr/lib/arm-linux-gnueabihf/weston-keyboard
     \_ /usr/lib/arm-linux-gnueabihf/weston-desktop-shell
         \_ /bin/bash /usr/local/bin/run-cog
             \_ cog -P fdo https://www.igalia.com/project/wpe
                 \_ /usr/lib/arm-linux-gnueabihf/wpe-webkit-1.0/WPEWebProcess 7 19
                 \_ /usr/lib/arm-linux-gnueabihf/wpe-webkit-1.0/WPENetworkProcess 8 19
```

## Using Cog

We have customized the Weston panel bar to provide a
sort of easy shortcuts to run the more common Cog:

![Weston panel](/data/images/example-2.png "Weston panel")

.. but you can also run those actions directly from the shell.


### Using Cog from the command line

You can manage Cog in remote or from the command line.

For a remote connection, you need first to activate the SSH server
and set a valid password for the user `pi`:

```
$ sudo -s
# passwd pi
Password: XXXXXXXX
# systemctl start ssh
```

* Getting help:

  ```
  $ cogctl help
  Available commands:
  appid      Display application ID being remotely controlled
  objpath    Display the D-Bus object path being used
  help       Obtain help about commands
  open       Open an URL
  previous   Navigate backward in the page view history
  next       Navigate forward in the page view history
  ping       Check whether Cog is running
  quit       Exit the application
  reload     Reload the current page
  ```

* Launching the Weston environment in the current X11 session:

  ```
  ssh pi@raspberry
  export DISPLAY=:0
  run-weston-x11
  ```

* Launching Cog in Weston over X11:

  ```
  ssh pi@raspberry
  export DISPLAY=:0
  run-cog
  ```

* Launching Weston:

  ```
  ssh pi@raspberry
  export DISPLAY=:0
  run-weston
  ```

* Launching Cog in Weston:

  ```
  ssh pi@raspberry
  export WAYLAND_DISPLAY="wayland-0"
  export XDG_RUNTIME_DIR="/run/user/1000/"
  run-cog
  ```

* Open a page in Cog

  ```
  ssh pi@raspberry
  cogctl open http://webkit.org/wpe/
  ```


## Remote Web Inspector

The [Web Inspector](https://trac.webkit.org/wiki/WebInspector) allows you
to view the page source, live DOM hierarchy, script debugging, profiling
and more!

The image contains a Web Inspector Viewer in local what can be launched
in the command line with the command `cog-remote-inspector-viewer` or
by pressing the *search* icon in the Weston panel:

![Web Inspector](/data/images/example-3.png "Web Inspector")


For the WebKit WPE port, this can be enabled with the
`WEBKIT_INSPECTOR_SERVER=${your raspberry pi IP address}:12321` environment
variable. This Raspbian image set this environment variable enabled
by default. To remotely connect to the Web Inspector:


```
epiphany-browser inspector://<your raspberry pi IP address>:12321
```

![Remote Web Inspector](/data/images/example-4.png "Remote Web Inspector")

## Keyboard shortcuts

### Weston shortcuts

* `Super+Tab`: Change active window

### LXDE shortcuts

* `Alt+Tab`: Change active window
